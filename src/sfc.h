/* sfc.h
 *
 * Copyright 2019 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

#define SFC_INSIDE
# include "sfc-version.h"
#undef SFC_INSIDE


typedef enum {
	SFC_E_OK,
	SFC_E_MISSING_ARG,
	SFC_E_DECOMPRESS_FAILED,
	SFC_E_JSON_MALFORMED,
	SFC_E_WRONG_VERSION,
	SFC_E_WRONG_COST,
	SFC_E_PBKDF2_HMAC_FAILED,
	SFC_E_NO_KEYS,
	SFC_E_RAND,
	SFC_E_BAD_ENCRYPT,
	SFC_E_CURL = 100,
	SFC_E_RESP_CODE = 200,
} sfc_ret_t;

#define IS_E_CURL(x)		((x) < (-SFC_E_CURL) && (x) > (-SFC_E_RESP_CODE))
#define IS_E_RESP_CODE(x)	((x) < (_SFC_E_RESP_CODE))

#define CURL_TO_SFC_E(x)	(SFC_E_CURL + (x))
#define SFC_TO_CURL_E(x)	-(SFC_E_CURL + (x))

#define RESP_CODE_TO_SFC_E(x)	(SFC_E_RESP_CODE + (x))
#define SFC_TO_RESP_CODE_E(x)	-(SFC_E_RESP_CODE + (x))

struct sfc;

extern struct sfc *sfc_open(const gchar *url_server);
extern void sfc_close(struct sfc *sfc);
extern sfc_ret_t sfc_generate_keys(struct sfc *sfc, const gchar *email, const gchar *password);
extern sfc_ret_t sfc_auth_sign_in(struct sfc *sfc);
extern sfc_ret_t sfc_sync(struct sfc *sfc);
extern sfc_ret_t sfc_create_note(struct sfc *sfc, const gchar *title, const gchar *text);

G_END_DECLS
