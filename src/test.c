#include <stdio.h>
#include <unistd.h>
#include "sfc.h"

int main(void)
{
	struct sfc *sfc;
	sfc_ret_t ret;


	sfc = sfc_open("https://sync.standardnotes.org");
	if (!sfc) {
		fprintf(stderr, "Error in sfc_open()");
		exit(EXIT_FAILURE);
	}

	if ((ret = sfc_generate_keys(sfc, "XXXXXXXXXX@gmail.com", "XXXXXXXX")) != SFC_E_OK) {
		fprintf(stderr, "Error in sfc_generate_keys(): %d\n", ret);
		exit(EXIT_FAILURE);
	}

	if ((sfc_create_note(sfc, "Note title", "This is a sample text")) != SFC_E_OK) {
		fprintf(stderr, "Error in sfc_generate_keys(): %d\n", ret);
		exit(EXIT_FAILURE);
	}

	if ((ret = sfc_auth_sign_in(sfc)) != SFC_E_OK) {
		fprintf(stderr, "Error signin: %d\n", ret);
		exit(EXIT_FAILURE);
	}

	while (1) {
		if ((ret = sfc_sync(sfc)) != SFC_E_OK) {
			fprintf(stderr, "Error sync: %d\n", ret);
			exit (EXIT_FAILURE);
		}
		sleep(5);
	}

	sfc_close(sfc);
}
