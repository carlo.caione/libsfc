/* sfc.c
 *
 * Copyright 2019 Carlo Caione
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <curl/curl.h>
#include <gio/gio.h>
#include <json-glib/json-glib.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/rand.h>

#include "sfc.h"

#define AUTH_CODE_FAIL		500
#define AUTH_CODE_PARAMS_OK	200
#define AUTH_LATEST_VER		"003"
#define AUTH_MIN_COST		100000

#define KEY_LEN			96

#define DEBUG

struct auth_param {
	gchar *identifier;
	gchar *pw_nonce;
	gchar *version;
	guint pw_cost;
};

struct key {
	gchar *pw;
	gchar *mk;
	gchar *ak;
};

struct item {
	JsonNode *json;
	JsonNode *json_content;
	gchar *uuid;
	gboolean updated;
};

struct sfc {
	CURL *curl;
	gchar *url_server;
	gchar *sync_token;
	gchar *jwt;
	struct auth_param param;
	struct key k;
	GHashTable *items;
	GHashTable *dirty_items;
};

static size_t get_data_cb(void *buffer, size_t size, size_t nmemb, void *userp)
{
	GByteArray *buf = userp;
	size_t real_size;

	real_size = size * nmemb;
	g_byte_array_append(buf, (guint8 *) buffer, real_size);

	return real_size;
}

static GBytes *decompress_data(GByteArray *zdata)
{
	GZlibDecompressor *decompressor;
	gsize b_read, b_write;
	GConverterResult res;
	GByteArray *output;
	guint8 tmp[256];
	gsize size;

	size = 0;
	output = g_byte_array_new();
	decompressor = g_zlib_decompressor_new(G_ZLIB_COMPRESSOR_FORMAT_GZIP);

	while (TRUE) {
		b_read = 0;
		b_write = 0;

		res = g_converter_convert(G_CONVERTER(decompressor),
					  zdata->data + size, zdata->len - size,
					  tmp, sizeof(tmp), G_CONVERTER_INPUT_AT_END,
					  &b_read, &b_write, NULL);

		switch (res) {
		case G_CONVERTER_CONVERTED:
			g_byte_array_append(output, tmp, b_write);
			size += b_read;
			break;
		case G_CONVERTER_FINISHED:
			g_byte_array_append(output, tmp, b_write);
			return g_byte_array_free_to_bytes(output);
		case G_CONVERTER_ERROR:
			g_byte_array_free(output, TRUE);
			return NULL;
		default:
			break;
		}
	};
}

static gboolean load_json(GBytes *data, JsonParser *parser)
{
	gconstpointer p_data;
	gsize p_size;

	p_data = g_bytes_get_data(data, &p_size);

	return json_parser_load_from_data(parser, p_data, p_size, NULL);
}

static sfc_ret_t do_request(struct sfc *sfc, gchar *request, gchar *post_fields,
			    JsonParser *parser)
{
	g_autoptr(GByteArray) buf_reply = NULL;
	struct curl_slist *header = NULL;
	g_autoptr(GBytes) data = NULL;
	g_autofree gchar *auth = NULL;
	long response_code;
	CURLcode res;

	buf_reply = g_byte_array_new();

	curl_easy_reset(sfc->curl);

	curl_easy_setopt(sfc->curl, CURLOPT_URL, request);
	curl_easy_setopt(sfc->curl, CURLOPT_WRITEDATA, buf_reply);
	curl_easy_setopt(sfc->curl, CURLOPT_WRITEFUNCTION, get_data_cb);

	if (post_fields)
		curl_easy_setopt(sfc->curl, CURLOPT_POSTFIELDS, post_fields);

	if (sfc->sync_token)
		header = curl_slist_append(header, "Content-Type: application/json; charset=utf-8");

	if (sfc->jwt) {
		auth = g_strdup_printf("Authorization: Bearer %s", sfc->jwt);
		header = curl_slist_append(header, auth);
		curl_easy_setopt(sfc->curl, CURLOPT_HTTPHEADER, header);
	}

	if ((res = curl_easy_perform(sfc->curl)) != CURLE_OK) {
		curl_slist_free_all(header);
		return -CURL_TO_SFC_E(res);
	}

	curl_slist_free_all(header);

	curl_easy_getinfo(sfc->curl, CURLINFO_RESPONSE_CODE, &response_code);
	if (response_code != AUTH_CODE_PARAMS_OK)
		return -RESP_CODE_TO_SFC_E(response_code);

	if (!(data = decompress_data(buf_reply)))
		return -SFC_E_DECOMPRESS_FAILED;

	if (!load_json(data, parser))
		return -SFC_E_JSON_MALFORMED;

#ifdef DEBUG
	printf("\n[JSON received by server]\n");
	printf("%s\n", json_to_string(json_parser_get_root(parser), TRUE));
	printf("[-----------------------]\n");
#endif

	return SFC_E_OK;
}

static const gchar *json_read_string(JsonNode *node, const gchar *member)
{
	JsonObject *obj;

	obj = json_node_get_object(node);

	return json_object_get_string_member(obj, member);
}

static gint64 json_read_int(JsonNode *node, const gchar *member)
{

	JsonObject *obj;

	obj = json_node_get_object(node);

	return json_object_get_int_member(obj, member);
}


static gboolean json_read_boolean(JsonNode *node, const gchar *member)
{
	JsonObject *obj;

	obj = json_node_get_object(node);

	return json_object_get_boolean_member(obj, member);
}

static sfc_ret_t parse_auth_params(struct sfc *sfc, JsonParser *parser)
{
	const gchar *version;
	JsonNode *node;
	gint64 cost;

	node = json_parser_get_root(parser);

	version = json_read_string(node, "version");
	if (strcmp(version, AUTH_LATEST_VER))
		return -SFC_E_WRONG_VERSION;

	cost = json_read_int(node, "pw_cost");
	if (cost < AUTH_MIN_COST)
		return -SFC_E_WRONG_COST;

	sfc->param.identifier = g_strdup(json_read_string(node, "identifier"));
	sfc->param.pw_nonce = g_strdup(json_read_string(node, "pw_nonce"));
	sfc->param.version = g_strdup(version);
	sfc->param.pw_cost = (guint) cost;

	return SFC_E_OK;
}

static void parse_auth_signin(struct sfc *sfc, JsonParser *parser)
{
	JsonNode *node;

	node = json_parser_get_root(parser);

	sfc->jwt = g_strdup(json_read_string(node, "token"));
}

static void parse_sync(struct sfc *sfc, JsonParser *parser)
{
	const gchar *type, *content, *uuid;
	JsonNode *node_root, *node;
	JsonObject *obj_root;
	JsonArray *array;
	gint i;

	node_root = json_parser_get_root(parser);
	obj_root = json_node_get_object(node_root);

	sfc->sync_token = g_strndup(json_read_string(node_root, "sync_token"), 28);

	array = json_object_get_array_member(obj_root, "retrieved_items");

	for (i = 0; i < json_array_get_length(array); i++) {
		struct item *item;

		item = g_new0(struct item, 1);

		node = json_array_get_element(array, i);

		uuid = json_read_string(node, "uuid");

		if (json_read_boolean(node, "deleted")) {
#ifdef DEBUG
			printf("\nDeleted %s\n", uuid);
#endif
			g_hash_table_remove(sfc->items, uuid);
			continue;
		}

		type = json_read_string(node, "content_type");
		if (g_strcmp0(type, "Note") && g_strcmp0(type, "Tag"))
			continue;

		content = json_read_string(node, "content");
		if (!g_str_has_prefix(content, AUTH_LATEST_VER))
			continue;

		item->json = json_node_copy(node);
		item->uuid = g_strdup(uuid);

		item->updated = TRUE;

#ifdef DEBUG
		printf("\nAdded/Modified [%s] %s\n", type, item->uuid);
		printf("[JSON encrypted]\n");
		printf("%s\n", json_to_string(item->json, TRUE));
#endif
		g_hash_table_replace(sfc->items, item->uuid, item);
	}
}

static gchar *hexlify(guchar *data, gsize data_size)
{
	gchar *hex;
	gint i;

	hex = g_malloc0(data_size * 2 + 1);

	for (i = 0; i < data_size; i++)
		g_snprintf(&hex[i * 2], 3, "%02x", data[i]);

	return hex;
}

static guchar *unhexlify(const gchar *hex, gsize len, gsize *data_size)
{
	guchar *data;
	gint i;

	*data_size = len / 2;
	data = g_malloc0(*data_size);

	for (i = 0; i < *data_size; i++)
		sscanf(&hex[i * 2], "%02hhx", &data[i]);

	return data;
}

static guchar *decrypt_content(const gchar *b64_ciphertext, const gchar *hex_key,
			       const gchar *hex_iv, gsize *plaintext_len)
{
	gsize key_len, iv_len, ciphertext_len;
	g_autofree guchar *ciphertext = NULL;
	g_autofree guchar *key = NULL;
	g_autofree guchar *iv = NULL;
	EVP_CIPHER_CTX *ctx;
	guchar *plaintext;
	int bytes_written;

	key = unhexlify(hex_key, strlen(hex_key), &key_len);
	iv = unhexlify(hex_iv, strlen(hex_iv), &iv_len);

	ciphertext = g_base64_decode(b64_ciphertext, &ciphertext_len);
	plaintext = g_malloc0(ciphertext_len);

	if (!(ctx = EVP_CIPHER_CTX_new()))
		return NULL;

	if (EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv) != 1)
		goto out;

	*plaintext_len = 0;
	if (!EVP_DecryptUpdate(ctx, plaintext, &bytes_written, ciphertext, ciphertext_len))
		goto out;
	*plaintext_len += bytes_written;

	if (EVP_DecryptFinal_ex(ctx, plaintext + bytes_written, &bytes_written) != 1)
		goto out;
	*plaintext_len += bytes_written;

	EVP_CIPHER_CTX_free(ctx);

	return plaintext;

out:
	*plaintext_len = 0;
	EVP_CIPHER_CTX_free(ctx);

	return NULL;
}

static gchar *decrypt(struct sfc *sfc, struct item *item,
		      const gchar *to_decrypt, const gchar *enc_key,
		      const gchar *auth_key, gsize *plaintext_len)
{
	const gchar *version, *auth_hash, *uuid, *IV, *ciphertext;
	g_autofree gchar *string_to_auth = NULL;
	g_autofree gchar *out_hex = NULL;
	g_autofree guchar *out = NULL;
	g_autofree guchar *key = NULL;
	unsigned int out_size;
	gchar **components;
	guchar *plaintext;
	gsize len;

	components = g_strsplit(to_decrypt, ":", -1);

	version = components[0];
	auth_hash = components[1];
	uuid = components[2];
	IV = components[3];
	ciphertext = components[4];

	if (g_strcmp0(item->uuid, uuid))
		goto out;

	string_to_auth = g_strjoin(":", version, uuid, IV, ciphertext, NULL);

	key = unhexlify(auth_key, strlen(auth_key), &len);
	out = g_malloc0(len);

	if (!HMAC(EVP_sha256(), key, len, (guchar *) string_to_auth,
		  strlen(string_to_auth), out, &out_size))
		goto out;

	out_hex = hexlify(out, out_size);

	if (g_strcmp0(out_hex, auth_hash))
		goto out;

	if (!(plaintext = decrypt_content(ciphertext, enc_key, IV, plaintext_len)))
		goto out;

	g_strfreev(components);

	return (gchar *) plaintext;

out:
	*plaintext_len = 0;
	g_strfreev(components);

	return NULL;
}

static void decrypt_items_cb(gpointer key, gpointer value, gpointer p_sfc)
{
	gsize item_key_len, json_content_len, split_len;
	g_autofree gchar *json_content = NULL;
	const gchar *content, *enc_item_key;
	g_autofree gchar *item_key = NULL;
	g_autofree gchar *item_ek = NULL;
	g_autofree gchar *item_ak = NULL;
	struct item *item;
	struct sfc *sfc;

	sfc = (struct sfc *) p_sfc;
	item = (struct item *) value;

	if (!item->updated)
		return;

	enc_item_key = json_read_string(item->json, "enc_item_key");
	content = json_read_string(item->json, "content");

	if (!(item_key = decrypt(sfc, item, enc_item_key, sfc->k.mk,
				 sfc->k.ak, &item_key_len)))
		return;

	split_len = item_key_len / 2;
	item_ek = g_strndup(&item_key[0], split_len);
	item_ak = g_strndup(&item_key[split_len], split_len);

	if (!(json_content = decrypt(sfc, item, content, item_ek,
					   item_ak, &json_content_len)))
		return;

	json_content[json_content_len] = 0;

	if (!(item->json_content = json_from_string(json_content, NULL)))
		return;

#ifdef DEBUG
	printf("\nDecrypted %s\n", item->uuid);
	printf("[JSON content decrypted]\n");
	printf("%s\n", json_to_string(item->json_content, TRUE));
#endif

	item->updated = FALSE;
}

static gchar *generate_salt(struct sfc *sfc)
{
	g_autofree gchar *salt = NULL;
	struct auth_param *p;

	p = &sfc->param;

	salt = g_strdup_printf("%s:SF:%s:%d:%s", p->identifier, p->version,
				p->pw_cost, p->pw_nonce);

	return g_compute_checksum_for_string(G_CHECKSUM_SHA256, salt, -1);
}

static sfc_ret_t generate_key_and_password(struct sfc *sfc, const gchar *password)
{
	g_autofree gchar *out_hex = NULL;
	g_autofree gchar *salt = NULL;
	const EVP_MD *sha_512;
	guchar out[KEY_LEN];
	gsize split_len;

	salt = generate_salt(sfc);

	sha_512 = EVP_sha512();

	if (!PKCS5_PBKDF2_HMAC(password, -1, (guchar *) salt, strlen(salt),
			       sfc->param.pw_cost, sha_512, KEY_LEN, out))
		return -SFC_E_PBKDF2_HMAC_FAILED;

	out_hex = hexlify(out, KEY_LEN);

	split_len = (KEY_LEN * 2) / 3;

	sfc->k.pw = g_strndup(&out_hex[0], split_len);
	sfc->k.mk = g_strndup(&out_hex[split_len], split_len);
	sfc->k.ak = g_strndup(&out_hex[split_len * 2], split_len);

	return SFC_E_OK;
}

static void key_destroy_func_cb(gpointer data)
{
	g_free(data);
}

static void value_destroy_func_cb(gpointer data)
{
	struct item *item;

	item = (struct item *) data;

	json_node_free(item->json_content);
	json_node_free(item->json);
}

static JsonNode *create_empty_item(const gchar *type)
{
	g_autoptr(JsonBuilder) builder = NULL;
	g_autofree gchar *cdate = NULL;
	g_autofree gchar *uuid = NULL;
	GTimeVal time;

	builder = json_builder_new();

	json_builder_begin_object(builder);

	json_builder_set_member_name(builder, "content");
	json_builder_add_null_value(builder);

	json_builder_set_member_name(builder, "content_type");
	json_builder_add_string_value(builder, type);

	json_builder_set_member_name(builder, "uuid");
	uuid = g_uuid_string_random();
	json_builder_add_string_value(builder, uuid);

	json_builder_set_member_name(builder, "created_at");
	g_get_current_time(&time);
	cdate = g_time_val_to_iso8601(&time);
	json_builder_add_string_value(builder, cdate);

	json_builder_set_member_name(builder, "deleted");
	json_builder_add_boolean_value(builder, FALSE);

	json_builder_end_object(builder);

	return json_builder_get_root(builder);
}

static JsonNode *create_empty_content(const gchar *title, const gchar *text)
{
	g_autoptr(JsonBuilder) builder = NULL;

	builder = json_builder_new();

	json_builder_begin_object(builder);

	json_builder_set_member_name(builder, "title");
	json_builder_add_string_value(builder, title);

	json_builder_set_member_name(builder, "text");
	json_builder_add_string_value(builder, text);

	json_builder_set_member_name(builder, "references");
	json_builder_begin_array(builder);
	json_builder_end_array(builder);

	json_builder_end_object(builder);

	return json_builder_get_root(builder);
}

static gchar *encrypt_content(const gchar *cleartext, const gchar *hex_key,
			      const gchar *hex_iv, gsize *ciphertext_len)
{
	g_autofree guchar *ciphertext = NULL;
	gsize key_len, iv_len, input_len;
	g_autofree guchar *key = NULL;
	g_autofree guchar *iv = NULL;
	EVP_CIPHER_CTX *ctx;
	int bytes_written;

	key = unhexlify(hex_key, strlen(hex_key), &key_len);
	iv = unhexlify(hex_iv, strlen(hex_iv), &iv_len);

	input_len = strlen(cleartext) + 1;
	ciphertext = g_malloc0(input_len + 16);

	if (!(ctx = EVP_CIPHER_CTX_new()))
		return NULL;

	if (EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv) != 1)
		goto out;

	*ciphertext_len = 0;
	if (!EVP_EncryptUpdate(ctx, ciphertext, &bytes_written, (guchar *) cleartext, input_len))
		goto out;
	*ciphertext_len += bytes_written;

	if (EVP_EncryptFinal_ex(ctx, ciphertext + bytes_written, &bytes_written) != 1)
		goto out;
	*ciphertext_len += bytes_written;

	EVP_CIPHER_CTX_free(ctx);

	return g_base64_encode(ciphertext, *ciphertext_len);

out:
	*ciphertext_len = 0;
	EVP_CIPHER_CTX_free(ctx);

	return NULL;
}

// TODO: ciphertext_len ??
static gchar *encrypt(struct sfc *sfc, struct item *item,
		      const gchar *to_encrypt, const gchar *enc_key,
		      const gchar *auth_key, gsize *ciphertext_len)
{
	g_autofree gchar *string_to_auth = NULL;
	g_autofree gchar *auth_hash_hex = NULL;
	g_autofree gchar *ciphertext = NULL;
	g_autofree guchar *auth_hash = NULL;
	g_autofree gchar *IV_hex = NULL;
	g_autofree guchar *key = NULL;
	unsigned int auth_hash_len;
	guchar IV[16];
	gsize key_len;

	if (!RAND_bytes(IV, 16))
		return NULL;

	IV_hex = hexlify(IV, sizeof(IV));

	if (!(ciphertext = encrypt_content(to_encrypt, enc_key, IV_hex, ciphertext_len)))
	    goto out;

	string_to_auth = g_strjoin(":", AUTH_LATEST_VER, item->uuid, IV_hex, ciphertext, NULL);

	key = unhexlify(auth_key, strlen(auth_key), &key_len);

	auth_hash = g_malloc0(key_len);

	if (!HMAC(EVP_sha256(), key, key_len, (guchar *) string_to_auth,
		  strlen(string_to_auth), auth_hash, &auth_hash_len))
		goto out;

	auth_hash_hex = hexlify(auth_hash, auth_hash_len);

	return g_strjoin(":", AUTH_LATEST_VER, auth_hash_hex, item->uuid, IV_hex, ciphertext, NULL);

out:
	*ciphertext_len = 0;

	return NULL;
}

// TODO: defines
static sfc_ret_t encrypt_item(struct sfc *sfc, struct item *item)
{
	gsize split_len, content_enc_len, item_key_enc_len;
	g_autofree gchar *content_to_encrypt = NULL;
	g_autofree gchar *item_key_hex = NULL;
	g_autofree gchar *item_key_enc = NULL;
	g_autofree gchar *item_ek = NULL;
	g_autofree gchar *item_ak = NULL;
	g_autofree gchar *content_enc = NULL;
	guchar item_key[64];
	JsonObject *obj;

	content_to_encrypt = json_to_string(item->json_content, FALSE);

	obj = json_node_get_object(item->json);

	if (!RAND_bytes(item_key, sizeof(item_key)))
		return -SFC_E_RAND;

	item_key_hex = hexlify(item_key, sizeof(item_key));

	split_len = strlen(item_key_hex) / 2;
	item_ek = g_strndup(&item_key_hex[0], split_len);
	item_ak = g_strndup(&item_key_hex[split_len], split_len);

	if (!(content_enc = encrypt(sfc, item, content_to_encrypt, item_ek, item_ak, &content_enc_len)))
		return -SFC_E_BAD_ENCRYPT;
	json_object_set_string_member(obj, "content", content_enc);

	if (!(item_key_enc = encrypt(sfc, item, item_key_hex, sfc->k.mk, sfc->k.ak, &item_key_enc_len)))
		return -SFC_E_BAD_ENCRYPT;
	json_object_set_string_member(obj, "enc_item_key", item_key_enc);

	return SFC_E_OK;
}

static gchar *get_dirty_items(struct sfc *sfc)
{
	g_autoptr(JsonBuilder) builder = NULL;
	g_autoptr(JsonNode) node = NULL;
	GHashTableIter iter;
	gpointer key, value;
	JsonArray* array;
	gchar *ret;

	node = json_node_new(JSON_NODE_ARRAY);
	array = json_array_new();

	g_hash_table_iter_init (&iter, sfc->dirty_items);
	while (g_hash_table_iter_next (&iter, &key, &value)) {
		struct item *item = value;

		json_array_add_element(array, item->json);
#ifdef DEBUG
		printf("\n[Sending %s]\n", item->uuid);
		printf("%s\n", json_to_string(item->json, TRUE));
		printf("%s\n", json_to_string(item->json_content, TRUE));
#endif
	}

	json_node_set_array(node, array);

	builder = json_builder_new();

	json_builder_begin_object(builder);

	json_builder_set_member_name(builder, "sync_token");
	json_builder_add_string_value(builder, sfc->sync_token);

	json_builder_set_member_name(builder, "items");
	json_builder_add_value(builder, node);

	json_builder_end_object(builder);

#ifdef DEBUG
	printf("\n[Sending]\n");
	printf("%s\n", json_to_string(json_builder_get_root(builder), TRUE));
#endif
	ret = json_to_string(json_builder_get_root(builder), FALSE);

	g_hash_table_remove_all(sfc->dirty_items);

	return ret;
}


struct sfc *sfc_open(const gchar *server)
{
	struct sfc *sfc;

	if (!server)
		return NULL;

	sfc = g_new0(struct sfc, 1);

	if (!(sfc->curl = curl_easy_init())) {
		g_free(sfc);
		return NULL;
	}

	sfc->url_server = g_strdup(server);
	sfc->items = g_hash_table_new_full(g_str_hash, g_str_equal,
					   key_destroy_func_cb,
					   value_destroy_func_cb);
	sfc->dirty_items = g_hash_table_new_full(g_str_hash, g_str_equal,
						 key_destroy_func_cb,
						 value_destroy_func_cb);
	return sfc;
}

void sfc_close(struct sfc *sfc)
{
	curl_easy_cleanup(sfc->curl);

	g_free(sfc->url_server);
	g_free(sfc->sync_token);
	g_free(sfc->jwt);

	g_free(sfc->param.identifier);
	g_free(sfc->param.pw_nonce);
	g_free(sfc->param.version);

	g_free(sfc->k.pw);
	g_free(sfc->k.mk);
	g_free(sfc->k.ak);

	g_hash_table_destroy(sfc->items);
	g_hash_table_destroy(sfc->dirty_items);

	g_free(sfc);
}

sfc_ret_t sfc_generate_keys(struct sfc *sfc, const gchar *email, const gchar *password)
{
	g_autoptr(JsonParser) parser = NULL;
	g_autofree gchar *query = NULL;
	sfc_ret_t ret;

	if (!email || !password)
		return -SFC_E_MISSING_ARG;

	parser = json_parser_new();

	query = g_strdup_printf("%s/auth/params?email=%s", sfc->url_server, email);

	if ((ret = do_request(sfc, query, NULL, parser)) != SFC_E_OK)
		return ret;

	if ((ret = parse_auth_params(sfc, parser)) != SFC_E_OK)
		return ret;

	if ((ret = generate_key_and_password(sfc, password)) != SFC_E_OK)
		return ret;

	return SFC_E_OK;
}

sfc_ret_t sfc_auth_sign_in(struct sfc *sfc)
{
	g_autofree gchar *post_fields = NULL;
	g_autoptr(JsonParser) parser = NULL;
	g_autofree gchar *query = NULL;
	sfc_ret_t ret;

	if (!sfc->k.pw || !sfc->k.mk || !sfc->k.ak)
		return -SFC_E_NO_KEYS;

	parser = json_parser_new();

	query = g_strdup_printf("%s/auth/sign_in", sfc->url_server);
	post_fields = g_strdup_printf("email=%s&password=%s", sfc->param.identifier, sfc->k.pw);

	if ((ret = do_request(sfc, query, post_fields, parser)) != SFC_E_OK)
		return ret;

	parse_auth_signin(sfc, parser);

	return SFC_E_OK;
}

sfc_ret_t sfc_sync(struct sfc *sfc)
{
	g_autofree gchar *post_fields = NULL;
	g_autoptr(JsonParser) parser = NULL;
	g_autofree gchar *query = NULL;
	sfc_ret_t ret;

	parser = json_parser_new();

	query = g_strdup_printf("%s/items/sync", sfc->url_server);
	post_fields = g_strdup("");

	if (sfc->sync_token)
		post_fields = g_strdup(get_dirty_items(sfc));

	if ((ret = do_request(sfc, query, post_fields, parser)) != SFC_E_OK)
		return ret;

	// TODO: unify decrypt into parse_sync?
	parse_sync(sfc, parser);

	g_hash_table_foreach(sfc->items, decrypt_items_cb, sfc);

	return SFC_E_OK;
}

sfc_ret_t sfc_create_note(struct sfc *sfc, const gchar *title, const gchar *text)
{
	struct item *item;
	sfc_ret_t ret;

	item = g_new0(struct item, 1);

	item->json = create_empty_item("Note");
	item->json_content = create_empty_content(title, text);
	item->uuid = g_strdup(json_read_string(item->json, "uuid"));

	if ((ret = encrypt_item(sfc, item)))
		return ret;

	g_hash_table_replace(sfc->dirty_items, item->uuid, item);

	return SFC_E_OK;

}
